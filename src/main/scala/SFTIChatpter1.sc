import scala.math._
import scala.util._

3.to(10).toString()

sqrt(2)

"crazy ".*(3)

pow(2, 3)
min(2, 3)
min(3, Pi)
max(3, Pi)

val x = 2
x * x * x

val str = "One"
str.toUpperCase
str.toLowerCase
str.toCharArray

pow(2, 1024)

BigInt(2).pow(1024)
BigInt.probablePrime(100, Random)

"one".charAt(0)
"one".reverse.charAt(0)
"one".take(1)
"one".takeRight(1)
"one".head

val one = "one"
one.drop(1)
one.dropRight(1)


