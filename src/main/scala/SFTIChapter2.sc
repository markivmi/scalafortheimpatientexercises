import scala.annotation.tailrec

val i = 0
if (i > 0) 1 else -1
val list = List.range(1, 3)

and(true, false)
and(true, true)
and(false, true)

def and(x: Boolean, y: Boolean) =
  if (x) y else false

factorial(4)

//Factorial with tail recursion
def factorial(n: Int): Int =
  if (n == 0) 1 else n * factorial(n - 1)

factorial1(4)

//Factorial without tail recursion
def factorial1(n: Int) = {
  def doFactorial(n: Int, res: Int): Int = {
    if (n == 0) res else doFactorial(n - 1, n * res)
  }
  doFactorial(n, 1)
}
println(matchTest(2))
//def sum(xs: List[Int]): Int = xs match {
//  case Nil => 0
//  case x :: tail => x + sum(tail)
//}

//def sum(xs: List[Int]) : Int = {
//  if(xs == Nil) 0
//  else xs.head + sum(xs.tail)
//}

def matchTest(x: Int): String = x match {
  case 1 => "one"
  case 2 => "two"
  case _ => "many"
}

//sum(1::2::3::Nil)

def sum(xs: List[Int]): Int = {
  @tailrec
  def doSum(xs: List[Int], res: Int): Int = xs match {
    case Nil => res
    case x :: tail => doSum(tail, res + x)
  }
  doSum(xs, 0)
}
println(sum(list))

