val x = "(x + y + (z + 1)))"

pascal(0, 2)
pascal(1, 2)
pascal(1, 3)
val y = "())("
val z = "(x + y + (z + 1))"
balance(x.toList)

def pascal(c: Int, r: Int): Int = {
  if (c == 0 || c == r) 1
  else pascal(c - 1, r - 1) + pascal(c, r - 1)
}
balance(y.toList)

def balance(chars: List[Char]): Boolean = {
  def f(chars: List[Char], open: Int): Boolean = {
    if (chars.isEmpty)
      open == 0
    else if (chars.head == '(')
      f(chars.tail, open + 1)
    else
      f(chars.tail, open)
  }

  f(chars, 0)
}
balance(z.toList)

