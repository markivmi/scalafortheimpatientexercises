val add2 = adder(_: Int, _: Int, 11)

sum(x => x, 3, 4)
sum(x => x * x, 3, 4)
val curriedAdd = (adder _).curried

sumInts(3, 5)

////
val addTwo = curriedAdd(2)

def sum(f: Int => Int, a: Int, b: Int): Int = {
  if (a > b) 0
  else f(a) + sum(f, a + 1, b)
}

sumInt2(3, 5)

///// sum using alternate syntax

def sumInts(a: Int, b: Int): Int = sum(x => x, a, b)

sum3(x => x)(3, 5)

def sum2(f: Int => Int): (Int, Int) => Int = {
  def sumF(a: Int, b: Int): Int = {
    if (a > b) 1
    else f(a) + sumF(a + 1, b)
  }
  sumF
}

product(x => x)(3, 4)

///////factorial using product

def sumInt2 = sum2(x => x)

factorial(4)

def sum3(f: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 0
  else f(a) + sum3(f)(a + 1, b)
}

////
def product(f: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 1
  else f(a) * product(f)(a + 1, b)
}

def factorial(n: Int) = product(x => x)(1, n)

add2(1, 3)

///
def mapReduce(f: Int => Int, combine: (Int, Int) => Int, Zero: Int)(a: Int, b: Int): Int = {
  if (a > b) Zero
  else combine(f(a), mapReduce(f, combine, Zero)(a + 1, b))
}

//// partial application
def adder(m: Int, n: Int, p: Int) = m + n + p

addTwo(4)